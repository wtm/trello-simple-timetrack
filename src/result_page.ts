import type { ResultObj } from "./result_obj";
declare var TrelloPowerUp: any;

let robj: ResultObj = TrelloPowerUp.iframe().arg("robj");
document.body.innerHTML = "";
let result = document.createElement("p");
let first = true;
function append_plus() {
	if (first) {
		first = false;
	} else {
		result.append(document.createTextNode(" + "));
	}
}
if (robj.d !== 0) {
	append_plus();
	result.append(document.createTextNode(robj.d.toString() + "d"));
}
if (robj.h !== 0) {
	append_plus();
	result.append(document.createTextNode(robj.h.toString() + "h"));
}
for (let uk of robj.unknowns) {
	append_plus();
	let qm = document.createElement("span");
	qm.classList.add("qm");
	qm.append(document.createTextNode("?"));
	qm.title = uk;
	result.append(qm);
}
document.body.append(result);
