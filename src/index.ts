declare var TrelloPowerUp: any;

import type { ResultObj } from "./result_obj"

TrelloPowerUp.initialize({
	"board-buttons": function(t, options) {
		return [{
			text: "Sum up times",
			condition: "always",
			callback: SumUpButtonCallback
		}];
	}
});

async function SumUpButtonCallback (t) {
	try {
		let cards: {name: string, desc: string}[] = await t.cards("name", "desc");
		let robj: ResultObj = {
			d: 0,
			h: 0,
			unknowns: []
		};
		for (let card of cards) {
			let time_strs = card.desc.match(/(?<=^| |\n)\d+(\.\d+)?[dh](?=$|\W)/g);
			if (!time_strs || time_strs.length === 0) {
				robj.unknowns.push(card.name);
			} else {
				let first = time_strs[0];
				let last_char = first.charAt(first.length - 1);
				let nb = parseFloat(first.substr(0, first.length - 1));
				if (last_char === 'd') {
					robj.d += nb;
				} else {
					robj.h += nb;
				}
			}
		}
		t.popup({
			title: "Sum up times",
			url: "sum-up-times-popup.html",
			args: {robj},
			height: 150
		});
	} catch (e) {
		console.error(e);
		t.popup({
			title: "Error",
			url: "error.html",
			args: {e: e.toString()},
			height: 100
		});
	}
}
