export type ResultObj = {
	d: number,
	h: number,
	unknowns: string[]
}
